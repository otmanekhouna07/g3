"""Main module for the streamlit app"""
import streamlit as st
import awesome_streamlit as ast
import pages.home
import pages.predict

st.set_page_config(page_title='PC Quality')

ast.core.services.other.set_logging_format()

PAGES = {
    "Home": pages.home,
    "Predict": pages.predict,
}
def main():

    st.markdown(f'<p style="color:#6a0dad;font-size:40px;text-align:center;"><strong>PC Quality Prediction Using Machine Learning Models</strong></p>', unsafe_allow_html=True)

    st.sidebar.markdown(f'<p style="color:#002e91;font-size:28px;"><strong>Machine Learning</strong></p>', unsafe_allow_html=True)

    selection = st.sidebar.radio("Menu", list(PAGES.keys()))
    page = PAGES[selection]
    with st.spinner(f"Loading {selection} ..."):
        ast.shared.components.write_page(page)
        
    st.sidebar.write("")
    st.sidebar.write("")
    st.sidebar.write("")

    st.sidebar.title("About")
    st.sidebar.info(" The goal is to classify whether a PC is GOOD or BAD Quality. To achieve this we have used machine learning classification methods to fit a function that can predict the discrete class of new input. ")

if __name__ == "__main__":
    main()
