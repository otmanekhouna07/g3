import streamlit as st

def write():
    st.write("Welcome to our web application that has a pretrained models that will be loaded when they will be selected.")

    st.write("Using Classification models for predicting PC quality")
    st.warning("""How to use it:  
    Click on Predict in Menu in the Sidebar then choose an algorithm and enter the 8 parameters, it will show you the result good or bad.""")
