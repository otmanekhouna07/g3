import streamlit as st
import awesome_streamlit as ast
import pages.home
import pages.knn
import pages.randomForest
import pages.gradientBoosting
import pages.decisionTree
import pages.naiveBayes
import pages.supportVect
import pages.logisticRegression

PAGES = {
    "Random Forest": pages.randomForest,
    "Logistic Regression": pages.logisticRegression,
    "K Nearest Neighbours": pages.knn,
    "Gradient Boosting": pages.gradientBoosting,
    "Decision Tree": pages.decisionTree,
    "Naive Bayes": pages.naiveBayes,
    "Support Vector Machine": pages.supportVect,
}

def write():

    # Display on webpage 
    st.sidebar.title("Steps for use: ")
    st.sidebar.write("1. Choose an algorithm from the list below")
    st.sidebar.write("2. Enter 8 values")
    st.sidebar.write("3. Press enter")
    st.sidebar.write("4. Wait for Good/Bad to appear")
    st.sidebar.write("It's that simple!")

    st.title("Algorithms")
    selection = st.radio("Select Algorithm", list(PAGES.keys()))
    page = PAGES[selection]
    with st.spinner(f"Loading {selection} ..."):
        ast.shared.components.write_page(page)

if __name__ == "__writeP__":
    writeP()